//
//  ViewController.swift
//  Invent Your Day
//
//  Created by Beautistar on 14/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import FSCalendar

class ViewController: UIViewController {

    fileprivate lazy var dateFormatter1: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    @IBOutlet weak var calendar: FSCalendar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        calendar.select(self.dateFormatter1.date(from: "2017/06/5"))
        calendar.select(self.dateFormatter1.date(from: "2017/06/10"))
        calendar.select(self.dateFormatter1.date(from: "2017/06/17"))
        calendar.select(self.dateFormatter1.date(from: "2017/06/23"))
        calendar.select(self.dateFormatter1.date(from: "2017/06/7"))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

