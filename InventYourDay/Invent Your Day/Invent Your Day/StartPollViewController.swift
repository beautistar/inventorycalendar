//
//  StartPollViewController.swift
//  Invent Your Day
//
//  Created by Beautistar on 14/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import FSCalendar

class StartPollViewController: UIViewController {
    
    @IBOutlet weak var calendar: FSCalendar!
    
    fileprivate lazy var dateFormatter1: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //initView()        
        
        calendar.select(self.dateFormatter1.date(from: "2017/06/13"))
        calendar.select(self.dateFormatter1.date(from: "2017/06/18"))
        calendar.select(self.dateFormatter1.date(from: "2017/06/25"))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //initView()
        
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        calendar.allowsSelection = false
        
    }
    @IBAction func closeAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }

}
