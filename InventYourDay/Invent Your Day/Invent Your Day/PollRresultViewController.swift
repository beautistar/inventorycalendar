//
//  PollRresultViewController.swift
//  Invent Your Day
//
//  Created by Beautistar on 14/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import FSCalendar

class PollRresultViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate, FSCalendarDelegateAppearance {
    
    fileprivate weak var calendar: FSCalendar!

    @IBOutlet weak var calendersb: FSCalendar!
    
    fileprivate let gregorian: Calendar = Calendar(identifier: .gregorian)
    var dateFormatter1: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()
    //var datesWithEvent = ["2015-10-03", "2015-10-06", "2015-10-12", "2015-10-25"]
    
    
    var fillDefaultColors = ["2017/06/08": UIColor.init(red: 254/255.0, green: 211/255.0, blue: 10/255.0, alpha: 1.0), "2017/06/06": UIColor.init(red: 83/255.0, green: 218/255.0, blue: 4/255.0, alpha: 1.0), "2017/06/18": UIColor.init(red: 254/255.0, green: 211/255.0, blue: 10/255.0, alpha: 1.0), "2017/06/22": UIColor.init(red: 254/255.0, green: 211/255.0, blue: 10/255.0, alpha: 1.0), "2017/07/08": UIColor.init(red: 254/255.0, green: 211/255.0, blue: 10/255.0, alpha: 1.0), "2017/07/06": UIColor.init(red: 83/255.0, green: 218/255.0, blue: 4/255.0, alpha: 1.0), "2017/07/18": UIColor.init(red: 254/255.0, green: 211/255.0, blue: 10/255.0, alpha: 1.0)]

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let view = UIView(frame: UIScreen.main.bounds)
        view.backgroundColor = UIColor.white
        self.view = view
        
        let label = UILabel(frame: CGRect(x:0, y:60, width:200, height:50))
        label.center = CGPoint(x:self.view.frame.size.width/2, y:85)
        label.textAlignment = NSTextAlignment.center
        
        label.font = UIFont(name:"Savoye LET", size:40)
        label.textColor = UIColor.red
        label.text = "Poll Result"
        self.view.addSubview(label)
        
        let btnRocket = UIButton(frame: CGRect(x: self.view.bounds.size.width - 60, y: 28, width: 40, height: 40))
        btnRocket.setImage(UIImage(named: "ic_close"), for: UIControlState.normal)
        btnRocket.addTarget(self, action: #selector(PollRresultViewController.closeAction), for:UIControlEvents.touchUpInside);
        self.view.addSubview(btnRocket)
        
        let height: CGFloat = UIDevice.current.model.hasPrefix("iPad") ? 450 : 300
        let calendar = FSCalendar(frame: CGRect(x:0, y:120, width:Int(self.view.bounds.size.width), height:Int(height)))
        calendar.dataSource = self
        calendar.delegate = self
        calendar.allowsSelection = false
        calendar.allowsMultipleSelection = false
        
        calendar.swipeToChooseGesture.isEnabled = true
        calendar.backgroundColor = UIColor.white
        calendar.appearance.caseOptions = [.headerUsesUpperCase,.weekdayUsesSingleUpperCase]
        calendar.appearance.todayColor = UIColor.init(red: 254/255.0, green: 211/255.0, blue: 10/255.0, alpha: 1.0)
        self.view.addSubview(calendar)
        self.calendar = calendar
        
        //calendar.select(self.dateFormatter1.date(from: "2017/6/15"))

        
        // For UITest
        self.calendar.accessibilityIdentifier = "calendar"
        
        let label1 = UILabel(frame: CGRect(x:20, y:calendar.frame.origin.y+calendar.frame.size.height+20, width:300, height:50))
        label1.textAlignment = NSTextAlignment.left
        
        label1.font = UIFont(name:"Savoye LET", size:40)
        label1.textColor = UIColor.blue
        label1.text = "More People = "
        self.view.addSubview(label1)
        
        let label2 = UILabel(frame: CGRect(x:40, y:label1.frame.origin.y+label1.frame.size.height, width:self.view.frame.size.width - 60, height:50))
        label2.textAlignment = NSTextAlignment.right
        
        label2.font = UIFont(name:"Savoye LET", size:40)
        label2.textColor = UIColor.blue
        label2.text = "More See"
        self.view.addSubview(label2)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func loadView() {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        
    }
    
    // MARK: - FSCalendar datasource
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, fillDefaultColorFor date: Date) -> UIColor? {
        let key = self.dateFormatter1.string(from: date)
        if let color = self.fillDefaultColors[key] {
            return color
        }
        return nil
    }
}
